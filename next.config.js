const webpack = require('webpack');
const withOptimizedImages = require('next-optimized-images');


const nextConfig = {
  webpack: (config, {dev}) => {
    config.plugins.push(new webpack.DefinePlugin({SC_DISABLE_SPEEDY: false}));
    return config;
  },
};

const optimizedImagesConfig = {
  inlineImageLimit: 8192,
  imagesFolder: 'images',
  imagesName: '[name]-[hash].[ext]',
  handleImages: ['jpeg', 'png', 'svg'],
  optimizeImages: true,
  optimizeImagesInDev: true,
  mozjpeg: {
    quality: 80,
  },
  optipng: {
    optimizationLevel: 3,
  },
  svgo: {
    plugins: [
      {
        removeViewBox: false,
      },
    ],
  },
};

module.exports = withOptimizedImages(optimizedImagesConfig, nextConfig);
