import React, {useState} from 'react'
import Head from 'next/head'
import NavWidget from "../src/components/organisms/NavWidget";
import styled from "styled-components";
import GlobalStyles from "../src/components/styles";
import HomeBlock from "../src/components/organisms/HomeBlock";
import AboutBlock from "../src/components/organisms/AboutBlock";
import SkillsBlock from "../src/components/organisms/SkillsBlock";
import WorksBlock from "../src/components/organisms/WorksBlock";
import ContactBlock from "../src/components/organisms/ContactBlock";

const HomePage = styled.div`
position: absolute;
width: 100%;
height: 100%;
`

export default function Home() {

  const [visibleBlock, setVisibleBlock] = useState('home');


  return (
    <>
      <Head>
        <title>ИндексJS</title>
        <link/>
      </Head>
      <GlobalStyles/>
      <NavWidget/>

      <HomePage>
        {
          visibleBlock === 'home' ? <HomeBlock/> :
            visibleBlock === 'about' ? <AboutBlock/> :
              visibleBlock === 'skills' ? <SkillsBlock/> :
                visibleBlock === 'works' ? <WorksBlock/> :
                  visibleBlock === 'contact' ? <ContactBlock/> : null
        }
      </HomePage>

    </>
  )
}
