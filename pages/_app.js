import React from 'react';
import GlobalStyles from "../src/components/styles";

const App = ({Component, pageProps}) => {
  return (
  <>
    <GlobalStyles/>
    <Component {...pageProps} />
  </>
  )
}

export default App;
