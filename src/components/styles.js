import {createGlobalStyle} from 'styled-components';
// import palette from './palette';
// import breakpoints from '../utils/constants/breakpoints';

const GlobalStyles = createGlobalStyle`
  html,body {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    background-color: #1d1d1d;
        
    &.ReactModal__Body--open {
      overflow: hidden;
    }
      }
  
  p {
    line-height: 1.5rem;
    margin: 0;
  }
* {
  box-sizing: border-box;
}

a {
  color: #0070f3;
  text-decoration: none;
}

a:hover {
  text-decoration: underline;
}

img {
  max-width: 100%;
  display: block;
}
`;

export default GlobalStyles;
