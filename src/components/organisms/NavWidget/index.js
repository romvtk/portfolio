import React from 'react';
import styled from "styled-components";
import NavBlock from "../../molecules/NavBlock";

const NavWidgetBlock = styled.div`
  position: absolute;
  width: 4.25rem;
  height: 100vh;
  background-color: #181818;
     `

const NavWidget = () => {
  return (
    <NavWidgetBlock>
      <NavBlock/>
    </NavWidgetBlock>

  )
}

export default NavWidget;
