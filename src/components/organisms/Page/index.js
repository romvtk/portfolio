import React from 'react';
import styled from "styled-components";
import AboutBlock from "../AboutBlock";
import HomeBlock from "../HomeBlock";
import SkillsBlock from "../SkillsBlock";
import WorksBlock from "../WorksBlock";
import ContactBlock from "../ContactBlock";

const PageBlock = styled.div`
position: absolute;
width: 100%;
height: 100%;
`
const Page = () => {
  return (
    <PageBlock>

    </PageBlock>
  )
}
export default Page
