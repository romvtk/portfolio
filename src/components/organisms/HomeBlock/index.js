import React, {useEffect} from 'react';
import styled from "styled-components";
import SpanTag from "../../atoms/SpanTag";

const WrapperHome = styled.div`
width: 100%;
height: 100%;
position: absolute;
border: 1px solid saddlebrown;
`
const RepresentBlock = styled.div`
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  left: 5rem;
  height: 200px;
  width: 45%;
  color: white;
    h1{
    color: 

  }
`
const RepresentParagraph = styled.p`
`

const HomeBlock = () => {

  useEffect(() => {
    let wrapper = document.getElementById('RepresentBlock');
    let text = document.querySelector('.RepresentParagraph');
    let h1 = document.createElement("h1");
    wrapper.appendChild(h1)
    let textContent = text.textContent;


  text.style.display = 'none';


    for (let i = 0; i < textContent.length; i++) {
      setTimeout(() => {
        let texts = document.createTextNode(textContent[i]);
        let span = document.createElement('span');
        if (i === 8 || i === 25) {
          const br = document.createElement('br')
          h1.appendChild(br)
        }
        span.appendChild(texts);
        h1.appendChild(span);
      }, 115 * i);

    }

  })


  return (
    <WrapperHome>
      <SpanTag text='<html>' left={6}/>
      <SpanTag text='<body>' left={7} top={2}/>
      <RepresentBlock id='RepresentBlock'>
        <RepresentParagraph className='RepresentParagraph'>
          Привет, Меня зовут Иван, Я Frontend developer
        </RepresentParagraph>

      </RepresentBlock>
      <SpanTag text='</body>' left={7} bottom={3}/>
      <SpanTag text='</html>' left={6} bottom={1}/>

    </WrapperHome>
  )
}

export default HomeBlock;
