import React from 'react';
import styled from "styled-components";

const Span = styled.span`
  position: absolute;
  color: gray;
  font-family: cursive;
  font-size: 1rem;
  left: ${({left}) => left && `${left}rem`};
  top :${({top}) => top && `${top}rem`};
  bottom :${({bottom}) => bottom && `${bottom}rem`};
`

const SpanTag = ({text, left, top, bottom}) => {
  return (
    <Span
      left={left}
      top={top}
      bottom={bottom}>
      {text}
    </Span>

  )
}

export default SpanTag;
