import React from 'react';
import styled from "styled-components";
import homeImage from '../../../components/atoms/img/home.svg'
import aboutImage from '../../../components/atoms/img/about.svg'
import skillsImage from '../../../components/atoms/img/skills.svg'
import worksImage from '../../../components/atoms/img/works.svg'
import contactImage from '../../../components/atoms/img/contact.svg'

const Nav = styled.nav`
  position: absolute;
  display: flex;
  width: 100%;
  top: 50%;
  transform: translateY(-50%);
  justify-content: space-between;
  flex-wrap: wrap;
  color:red;
 
    `
const NavLink = styled.div`
  position: relative;
  display: flex;
  width: 100%;
  height: 45px;
     
  text-align: center;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  //color: #4d4d4e;
  font-size: 12px;
     margin: 1rem 0;
    img {
      position: absolute;
      display: block;
     height: 45px;
      top: 50%;
      left: 50%;
   
      transform: translate3d(-50%,-50%,0);
    }
    
`

const NavBlock = () => {
  return (
    <Nav>
      <NavLink ><img src={homeImage} alt=""/></NavLink>
      <NavLink ><img src={aboutImage} alt=""/></NavLink>
      <NavLink ><img src={skillsImage} alt=""/></NavLink>
      <NavLink ><img src={worksImage} alt=""/></NavLink>
      <NavLink ><img src={contactImage} alt=""/></NavLink>




    </Nav>
  )
}

export default NavBlock;
